package com.example.thdworkapplication;

import android.content.Context;

import com.example.thdworkapplication.DataModels.Job;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public abstract class Supervisor {

    private DatabaseManager databaseManager;


    public void calulateDailyPercentage(List<Job> jobList, Context context) {
        databaseManager = DatabaseManager.getInstance(context);
        int total = 0;
        //Extract the variables
        int ber = 0; //This is half a repair (0.5)
        int repair = 0; // this counts as a full (1)
        int repairLite = 0; //This counts a 4th (0.5)
        int eval = 0; //This counts for nothing.
        int totalRepairsForTheDay = 0;
        //The assosciate must complete 10 repairs per day to maintain 100percent productivity.
        List<Job> data = databaseManager.getEverythingFromDatabase();

        for (Job temp : data) {
            ber = temp.getBer();
            repair = temp.getRepair();
            repairLite = temp.getRepairLite();
            eval = temp.getEval();

        }

        //adding the bers
        total += ber * 0.25;
        total += repairLite * 0.5;
        total += repair * 1;

    }
    private Calendar calendar;
    private SimpleDateFormat dateFormat;
    private String date;

    /**
     * Calculate the data and return the correct format
     * Format is as follows <bold>MM/DD/YYYY</bold>
     * @return
     */
    public String calculateYearToDate(){

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("MM/DD/YYYY") ;
        date = dateFormat.format(calendar.getTime());

        return date;
    }


}
