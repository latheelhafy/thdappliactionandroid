package com.example.thdworkapplication;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.thdworkapplication.DataModels.Job;

import java.util.ArrayList;

public class DataSource extends ArrayAdapter<Job> {

    private Context context;
    private ArrayList<Job> objects;

    public DataSource (Context context, int resource, ArrayList<Job> objects){
        super(context,resource,objects);
        this.context = context;
        this.objects = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        //Get the resource you want to display

        //Get the inflator to blow up the object on the screen.
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_previous,null);

        //Get the update data.

        return null; // will change when the application is finished.
    }
}
