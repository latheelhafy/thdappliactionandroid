package com.example.thdapplication;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thdapplication.DatabaseManager;
import com.example.thdworkapplication.DataModels.Job;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Extract all data that is passed into this avtivity to display to the user, this is the main feeder
 * for the data base and enteracting with the user. All data will be stored in the database for longer term use.
 */
public class CounterActivity extends AppCompatActivity {
    private int BER = 0;
    private int REPAIR_LITE = 0;

    private int REPAIR = 0;
    private int CUSTOMER = 0;
    private int STORE = 0;
    private String DATE = "";

    private static final String TAG = " Counter Activity ===> ";

    private DatabaseManager databaseManager;
    //Creating links to the buttons
    private EditText ber;
    private EditText repairLite;
    private EditText repair;
    private EditText customer;
    private EditText store;

    private Job viewBucket = new Job();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter);
        this.ber = (EditText) findViewById(R.id.BER);
        this.repairLite = (EditText) findViewById(R.id.repairLite);
        this.repair = (EditText) findViewById(R.id.repair);
        this.customer = (EditText) findViewById(R.id.customercount);
        this.store = (EditText) findViewById(R.id.storecount);

    }

    /**
     * This method will be called when all data is extracted and saved to the database successfully.
     * If the method is not called, then it will remain on the page allow the user to make adjustments were necessary.
     */
    private void moveBacktToMain() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    /**
     * Task list for this method.
     * Extract all the data from the interface,
     * Call Database to save the data to the records with the offical date code and time stamp.
     * Clear the fields and shift back.
     * <p>
     * TODO rewrite this method to for operational purposes.
     * <p>
     * The current problem is that the submit button is click and nothing happens.
     * Logical approach is that the viewbucket and the database manager can not be null
     * or the application should crash.
     *
     * @param view
     */
    public void extractEverythingFromInterface(View view) {
        Log.d("COUNTER CLASS ", "extract everything submit button selected. ");
        //Version 1.0 new code
        //Rewrite the logic for this method.
        databaseManager = DatabaseManager.getInstance(this);
//        if(viewBucket == null && databaseManager == null){ //If there is nothing from the previous modification
//            viewBucket = exractEverything();
//            databaseManager.createNewRecord(viewBucket);
//            clearGlobal();
//            moveBacktToMain();
//            toastMessage("a new record was created.");
//        } else { //If there is data left from before
//            toastMessage("The current record was added");
//            clearGlobal();
//        }
        viewBucket = extractEverything();
        databaseManager.createNewRecord(viewBucket);
        clearGlobal();
        toastMessage(databaseManager.howManyRecords() + "");
    }

    /**
     * Returning the string the current date extracted from the system.
     * This is going to be used to stamp the file created and as well as the heading of the
     * display so the user knows what record date they are looking at.
     *
     * @return
     */
    private String getDate() {
        Date date = Calendar.getInstance().getTime();

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-mm-yyyy");
        String formattedDate = dateFormatter.format(date);
        return dateFormatter.format(date);
    }

    /**
     * Check to see if there is a null pointer exception. The user shouldn't be able to use  the submit button
     * until the user has provided all the information.
     *
     * TODO : Check user data to see if it is valid or not.
     * TODO : Set default values for numbers that are not important to productivity.
     * TODO : Make sure that data passed into the application are valid.
     *
     * @return
     */
    private Job extractEverything() {
        Job temp = new Job();
        //Break pointq

        temp.setDate(getDate());
        try {
            temp.setBer(this.BER = Integer.parseInt(this.ber.getText().toString()));
            temp.setRepairLite(this.REPAIR_LITE = Integer.parseInt(this.repairLite.getText().toString()));
            temp.setRepair(this.REPAIR = Integer.parseInt(this.repair.getText().toString()));
            temp.setCustomer(this.CUSTOMER = Integer.parseInt(this.customer.getText().toString()));
            temp.setStore(this.STORE = Integer.parseInt(this.store.getText().toString()));
        } catch (NullPointerException e) {
            Log.d(TAG, "extractEverything: You can't leave some of the fields empty. All Fields must be given a default value.");
            toastMessage("You have to fill out all the fields!!!!");
        }
        return temp;
    }

    private void toastMessage(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        Log.d("CounterActivity ===>>>", "The count of record is : " + databaseManager.howManyRecords());
        toast.show();
    }

    private void clearGlobal() {
        this.DATE = "";
        this.BER = 0;
        this.REPAIR_LITE = 0;
        this.REPAIR = 0;
        this.CUSTOMER = 0;
        this.STORE = 0;
        this.viewBucket = null;
        clearTheUI();
    }

    private void clearTheUI() {
        this.ber.setText("");
        this.repairLite.setText("");
        this.repair.setText("");
        this.customer.setText("");
        this.store.setText("");

    }

}
