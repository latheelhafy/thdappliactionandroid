package com.example.thdworkapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.thdworkapplication.DataModels.Job;

import java.util.ArrayList;

public class CustomListAdapter extends BaseAdapter {
    private ArrayList<Job> listData;
    private LayoutInflater layoutInflater;


    public CustomListAdapter (Context context, ArrayList<Job> listData){
        this.listData = listData;
    }


    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        //Checking to see if the view is null or not
        if (convertView == null){
            //knowing which element to inflate on the list view.
            convertView = layoutInflater.inflate(R.layout.list_row,null);
            holder = new ViewHolder();
            //Extracting the exact element that will be inflated.
            holder.uName = (TextView) convertView.findViewById(R.id.cell);
            //If there is a null exception return the view bag as it is.
            holder = (ViewHolder) convertView.getTag();
        }

        holder.uName.setText(listData.get(position).getEval());
        return convertView;
    }

    /**
     * Holder class for holding the different parts of the view cell.
     * This class is helping customer the cell for item in the list view.
     */
    static class ViewHolder {
        TextView uName;
    }
}
